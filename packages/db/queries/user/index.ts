import { knex } from "knex";
import { config } from "../../knexfile";
import { User } from "../../../types/User";

const stageConfig = config[process.env.ENVIRONMENT ?? "local"];
const db = knex(stageConfig);

export const getAll = async () => {
  const users = await db("users");
  return users;
};

export const getById = async (id: string) => {
  const user = await db("users").where("user_id", id);
  return user;
};

export const getByEmail = async (email: string) => {
  const user = await db("users").where("email", email);
  return user;
};

export const create = async (user: User) => {
  const users = await db("users").insert(user);
  return users;
};

export const update = async (id, user: User) => {
  const date = new Date();
  const users = await db("users")
    .where("user_id", id)
    .update({
      full_name: user.full_name ?? "",
      birth: user.birth ?? "",
      full_address: user.full_address,
      identification: user.identification,
      role: user.role,
      pictures: user.pictures ?? "",
      documents: user.documents ?? "",
      updated_at: date.toISOString(),
      status: user.status,
    });
  return users;
};

export const deleteById = async (id) => {
  const users = await db("users").where("user_id", id).del();
  return users;
};
