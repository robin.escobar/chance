import { knex } from "knex";
import { config } from "../../knexfile";

const stageConfig = config[process.env.ENVIRONMENT ?? "local"];
const db = knex(stageConfig);

export const create = async (logins) => {
  const log = await db("logins").insert(logins);
  return log;
};
