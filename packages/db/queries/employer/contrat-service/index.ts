import { knexInstance as db } from "../../../";
import { ContractService } from "../../../../types/ContractService";

export const create = async (contractService: ContractService) => {
  const servi = await db("contract_service").insert(contractService);
  return servi;
};

export const deleteById = async (id: number) => {
  const servi = await db("contract_service")
    .where("contract_service_id", id)
    .del();
  return servi;
};

export const update = async (id: number, contractService: ContractService) => {
  const date = new Date();
  const servi = await db("contract_service")
    .where("contract_service_id", id)
    .update(contractService);

  return servi;
};

export const getAllContractServices = async () => {
  const servi = await db("contract_service");
  return servi;
};

export const getAllContractServicesForEmployeeID = async (userId: number) => {
  const servi = await db("contract_service").where(
    "fk_employee_user_id",
    userId
  );
  return servi;
};

export const getAllContractServicesForEmployerID = async (userId: number) => {
  const servi = await db("contract_service").where(
    "fk_employer_user_id",
    userId
  );
  return servi;
};
