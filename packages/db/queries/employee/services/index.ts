import { knex } from "knex";
import { config } from "../../../knexfile";
import { Service } from "../../../../types/Service";

const stageConfig = config[process.env.ENVIRONMENT ?? "local"];
const db = knex(stageConfig);

export const create = async (service: Service) => {
  const servi = await db("services").insert(service);
  return servi;
};

export const deleteById = async (id: number) => {
  const servi = await db("services").where("service_id", id).del();
  return servi;
};

export const update = async (id: number, service: Service) => {
  const date = new Date();
  const servi = await db("services")
    .where("service_id", id)
    .update({
      description: service.description ?? "",
      details: service.details ?? "",
      status: service.status ?? "",
      updated_at: date.toISOString(),
    });

  return servi;
};

export const getAllServices = async () => {
  const servi = await db("services");
  return servi;
};

export const getAllServicesForUserID = async (userId: number) => {
  const servi = await db("services").where("fk_user_id", userId);
  return servi;
};
