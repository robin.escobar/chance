import { knex } from "knex";
import { config } from "./knexfile";
import { ChanceMigrationSource } from "./migrations/";

const stageConfig = config[process.env.ENVIRONMENT ?? "local"];
export const knexInstance = knex(stageConfig);

export const migrateLatest = async () => {
  try {
    console.log("Env: ", process.env.ENVIRONMENT);
    console.log("migrate....: ", stageConfig);
    await knexInstance.migrate.latest({
      migrationSource: new ChanceMigrationSource(),
    });
    console.log(
      `${String.fromCodePoint(9989)} [Migrations]: migrations already executed`
    );
  } catch (err) {
    console.log("error", err);
  } finally {
    await knexInstance.destroy();
  }
};

export const rollback = async () => {
  try {
    console.log("Rollback....: ", stageConfig);
    const rollback: any = await knexInstance.migrate.rollback({
      migrationSource: new ChanceMigrationSource(),
    });
    console.log("Rollback response: ", { rollback });

    // await knexInstance.destroy();
  } catch (err) {
    console.log("error", err);
  } finally {
    await knexInstance.destroy();
  }
};

export const listMigrations = async () => {
  try {
    console.log("Migrations....: ", stageConfig);
    const migrations: any = await knexInstance.migrate.list({
      migrationSource: new ChanceMigrationSource(),
    });
    console.log("List Migrations: ", migrations);
    console.log(
      "Current Version: "
      // await knexInstance.migrate.currentVersion()
    );
    await knexInstance.destroy();
  } catch (err) {
    console.log("error", err);
  } finally {
    await knexInstance.destroy();
  }
};
