import type { Knex } from "knex";

// Update with your config settings.

export const config: { [key: string]: Knex.Config } = {
  dev: {
    client: "postgresql",
    connection: {
      database: "chance",
      user: "test",
      password: "test",
      host: "172.18.0.1",
      port: 5432,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "chance_migrations",
      // extension: ".ts",
      // directory:
      //   "/home/rescobar/Desktop/buildWithIn/otros2/BuildTurbo/packages/db-b2b-test/others",
    },
  },

  local: {
    client: "postgresql",
    connection: {
      host: "chance.cluster-cx5epkziqpt7.us-east-1.rds.amazonaws.com",
      port: 5432,
      database: "chance_db",
      user: "chance_user",
      password: "chance2023",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "chance_migrations",
      // extension: ".ts",
      // directory:
      //   "/home/rescobar/Desktop/buildWithIn/otros2/BuildTurbo/packages/db-b2b-test/others",
    },
  },
};
