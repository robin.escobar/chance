import { Knex } from "knex";

//https://knexjs.org/guide/migrations.html#custom-migration-sources
export class ChanceMigrationSource {
  getMigrations() {
    // In this example we are just returning migration names
    return Promise.resolve([
      "users",
      "logins",
      "type_service",
      "services",
      "type_sub_service",
      "sub_services",
      "contract_service",
    ]);
  }

  getMigrationName(migration: any) {
    return migration;
  }

  getMigration(migration: any) {
    switch (migration) {
      case "users":
        return {
          up(knex: Knex) {
            return knex.schema.createTable("users", function (table) {
              table.increments("user_id").primary();
              table.string("full_name").nullable();
              table.string("birth").nullable();
              table.string("full_address").nullable();
              table.string("identification").nullable().unique();
              table.string("pictures").nullable();
              table.string("documents").nullable();
              table.string("role").notNullable();
              table.integer("phone_number").nullable().unique();
              table.string("pass").notNullable();
              table.boolean("status").defaultTo(false); //si es completo o no
              table.integer("verification_code", 6).nullable();
              table.boolean("number_or_email_verified").defaultTo(false);
              table.string("email").notNullable().unique();
              table
                .string("avatar")
                .defaultTo(
                  "https://thumbs.dreamstime.com/b/icono-de-la-muestra-del-usuario-s%C3%ADmbolo-de-la-persona-avatar-humano-84519083.jpg"
                );
              table.timestamp("created_at").defaultTo(knex.fn.now());
              table.timestamp("updated_at").defaultTo(knex.fn.now());
            });
          },
          down(knex: Knex) {
            return knex.schema.dropTable("users");
          },
        };

      case "logins":
        return {
          up(knex: Knex) {
            return knex.schema.createTable("logins", function (table) {
              table.increments("login_id").primary();
              table.string("token", 5000).nullable();
              table.integer("user_id").nullable();
              table.timestamp("created_at").defaultTo(knex.fn.now());
              table.timestamp("updated_at").defaultTo(knex.fn.now());
            });
          },
          down(knex: Knex) {
            return knex.schema.dropTable("logins");
          },
        };
      //Types of services: "Jardineria, Albañileria, Niñera, "
      case "type_service":
        return {
          up(knex: Knex) {
            return knex.schema.createTable("type_service", function (table) {
              table.increments("type_service_id").primary();
              table.string("name", 5000).nullable();
              table.string("picture").nullable();
              table.timestamp("created_at").defaultTo(knex.fn.now());
              table.timestamp("updated_at").defaultTo(knex.fn.now());
            });
          },
          down(knex: Knex) {
            return knex.schema.dropTable("type_service");
          },
        };
      case "services":
        return {
          up(knex: Knex) {
            return knex.schema.createTable("services", function (table) {
              table.increments("service_id").primary();
              table.string("description", 5000).nullable();
              table.string("details", 5000).nullable();
              table.integer("fk_user_id").notNullable();
              table.integer("fk_type_service_id").notNullable();
              table.string("status").defaultTo("INITIATED"); //saber si esta iniciado, publicado, ejecutandose
              table.timestamp("created_at").defaultTo(knex.fn.now());
              table.timestamp("updated_at").defaultTo(knex.fn.now());
              //Fk
              table
                .foreign("fk_user_id")
                .references("user_id")
                .inTable("users");
              table
                .foreign("fk_type_service_id")
                .references("type_service_id")
                .inTable("type_service");
            });
          },
          down(knex: Knex) {
            return knex.schema.dropTable("services");
          },
        };
      //Guadar tipos de subservicios, segun un servicio, Servicio = Jardineria, subservicio = cortar cesped, sembrar flores
      case "type_sub_service":
        return {
          up(knex: Knex) {
            return knex.schema.createTable(
              "type_sub_service",
              function (table) {
                table.increments("type_sub_service_id").primary();
                table.integer("fk_type_service_id").notNullable();
                table.string("name").nullable();
                table.string("picture").nullable();
                table.timestamp("created_at").defaultTo(knex.fn.now());
                table.timestamp("updated_at").defaultTo(knex.fn.now());
                //Fk
                table
                  .foreign("fk_type_service_id")
                  .references("type_service_id")
                  .inTable("type_service");
              }
            );
          },
          down(knex: Knex) {
            return knex.schema.dropTable("type_sub_service");
          },
        };
      case "sub_services":
        return {
          up(knex: Knex) {
            return knex.schema.createTable("sub_services", function (table) {
              table.increments("sub_service_id").primary();
              table.integer("fk_type_sub_service_id").notNullable();
              table.integer("fk_service_id").notNullable();
              table.string("price").nullable;
              table.string("description").nullable();
              table.timestamp("created_at").defaultTo(knex.fn.now());
              table.timestamp("updated_at").defaultTo(knex.fn.now());
              //Fk
              table
                .foreign("fk_type_sub_service_id")
                .references("type_sub_service_id")
                .inTable("type_sub_service");
              table
                .foreign("fk_service_id")
                .references("service_id")
                .inTable("services");
            });
          },
          down(knex: Knex) {
            return knex.schema.dropTable("sub_services");
          },
        };

      case "contract_service":
        return {
          up(knex: Knex) {
            return knex.schema.createTable(
              "contract_service",
              function (table) {
                table.increments("contract_service_id").primary();
                table.integer("fk_employee_user_id").notNullable();
                table.integer("fk_employer_user_id").notNullable();
                table.integer("fk_service_id").notNullable();
                table.string("status").defaultTo("INITIAD");
                table.string("subservices_id").nullable(); //json de subservices
                table.boolean("is_custom_service").defaultTo(false); //sino elige el algun sub servicio, puede customizar uno
                table.integer("price_service").notNullable;
                table.boolean("automatic_payment").defaultTo(false);
                table.string("include").nullable(); //Array con lo que puede incluir
                table.timestamp("init_at").notNullable();
                table.timestamp("finish_at").notNullable();
                table.timestamp("contracted_at").defaultTo(knex.fn.now());
                table.timestamp("updated_at").defaultTo(knex.fn.now());
                //Fk
                table
                  .foreign("fk_employee_user_id")
                  .references("user_id")
                  .inTable("users");
                table
                  .foreign("fk_employer_user_id")
                  .references("user_id")
                  .inTable("users");
                table
                  .foreign("fk_service_id")
                  .references("service_id")
                  .inTable("services");
              }
            );
          },
          down(knex: Knex) {
            return knex.schema.dropTable("contract_service");
          },
        };
    }
  }
}
