export interface Service {
  readonly service_id?: number;
  description?: string;
  details?: string;
  fk_user_id?: number;
  fk_type_service_id?: number;
  status?: string;
  readonly created_at?: string;
  readonly updated_at?: string;
}
