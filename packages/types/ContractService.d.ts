export interface ContractService {
  readonly contract_service_id?: number;
  fk_employee_user_id?: number;
  fk_employer_user_id?: number;
  fk_service_id?: number;
  subservices_id?: string;
  is_custom_service?: boolean;
  price_service: string;
  automatic_payment?: boolean;
  include?: string[]; //que incluye el servicio si es custom
  status?: string;
  init_at?: string;
  finish_at?: string;
  readonly contracted_at?: string;
  readonly updated_at?: string;
}
