export interface User {
  readonly user_id?: number;
  phone_number?: number;
  full_name?: string;
  birth?: string;
  full_address?: string;
  identification?: string;
  pictures?: string;
  documents?: string;
  role?: RolEnum;
  pass?: string;
  email?: string;
  avatar?: string;
  status?: boolean;
  number_or_email_verified?: boolean;
  verification_code?: number;
  readonly created_at?: string;
  readonly updated_at?: string;
}

declare enum RolEnum {
  PERSON = "PERSON",
  EMPLOYER = "EMPLOYER",
  ADMINISTRATOR = "ADMINISTRATOR",
  SELLER = "SELLER",
}

export type UserModelKey = "USER";
