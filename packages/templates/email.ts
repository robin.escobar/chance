export const getUserTemplate = async (
  email: string,
  verificationCode: number
) => {
  try {
    return {
      from: "delivery@chance.com.gt",
      to: email,
      subject: "Ejemplo de asunto de correo",
      text: "Plaintext version of the message",
      html: `
<html>
  <head>
    <style>
      body {
        display: flex;
        justify-content: center;
      }
      .main {
        box-shadow: 2px 2px 5px #c2bdbc;
        border-radius: 20px;
        width: 550px;
        height: 200px;
        border: 1px solid #c2bdbc;
      }
      .head {
        margin: 20px 0 0 20px;
        width: 100%;
      }

      .line {
        border-top: 2px solid #d8d4d3;
        margin: 0px 25px 0 75px;
      }

      .avatar {
        vertical-align: middle;
        width: 50px;
        height: 35px;
        border-radius: 50%;
      }

      .taskicon {
        vertical-align: middle;
        width: 30px;
        height: 30px;
        border-radius: 50%;
      }

      table {
        margin: 30px 0 0 50px;
        table-layout: fixed;
        width: 500px;
        border-collapse: separate;
        border-spacing: 10px 5px;
      }

      div.center {
        margin-top: 50px;
        text-align: center;
      }
      .btn {
        text-decoration: none;
        box-shadow: 2px 2px 5px #c2bdbc;
        border-radius: 10px;
        border: none;
        background-color: black;
        padding: 14px 28px;
        font-size: 16px;
        cursor: pointer;
        display: inline-block;
      }

      .btntag {
        border-radius: 10px;
        border: none;
        background-color: #dbb8b8;
        padding: 3px 6px;
        font-size: 12px;
        cursor: pointer;
        display: inline-block;
        margin-top: 5px;
      }

      .btn:hover {
        background-color: #75716f;
        color: white;
      }
    </style>
  </head>
  <body>
    <div class="main">
      <div class="head">
        <img  src="https://i.postimg.cc/ZqmXfywg/Whats-App-Image-2023-01-31-at-5-09-11-PM.jpg"  alt="Avatar" class="avatar" />
        <label><b> Te damos la bienvenida a Chance</b></label>
      </div>
      <div class="line"></div>

      <table>
        <tr>
          <td style="width: 20%; color: #75716f">
            <b>Codigo de verificación</b>
          </td>
          <td style="width: 80%"><h2>${verificationCode}</h2></td>
        </tr>
      </table>
    </div>
  </body>
</html>`,
    };
  } catch (e) {
    console.error("Error getting template", {
      error: e,
    });
    return false;
  }
};
