const bcrypt = require("bcryptjs");
export const encrypt = async (text: string, rondal: number) => {
  try {
    return await bcrypt.hashSync(text, rondal);
  } catch (e) {
    console.error("Error creating token", {
      error: e,
    });
    return "Error creating token";
  }
};

export const compare = async (text: string, textEncripted: string) => {
  try {
    const response = await bcrypt.compareSync(text, textEncripted);
    return response;
  } catch (e) {
    console.error("Error verifiying badges", {
      error: e,
    });
    return false;
  }
};
