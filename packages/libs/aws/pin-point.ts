import {
  PinpointClient,
  SendMessagesCommand,
  SendMessagesCommandInput,
  SendMessagesCommandOutput,
} from "@aws-sdk/client-pinpoint";

const pinpointClient = new PinpointClient({ region: "us-east-1" });

export const sendPinpointSMS = async (
  input: SendMessagesCommandInput
): Promise<SendMessagesCommandOutput> => {
  try {
    const sendSMSCommand = new SendMessagesCommand(input);
    return await pinpointClient.send(sendSMSCommand);
  } catch (error) {
    console.error({
      error: "Failed to send pinpoint SMS",
      message: error,
      input: input,
    });
    throw new Error("Error sending pinpoint SMS");
  }
};
