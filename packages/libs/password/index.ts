const generator = require("generate-password");

export const generatePassword = async () => {
  try {
    const password = generator.generate({
      length: 10,
      numbers: true,
    });

    return password;
  } catch (e) {
    console.error("Error building password", {
      error: e,
    });
    return false;
  }
};
