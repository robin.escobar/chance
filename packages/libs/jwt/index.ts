const jwt = require("jsonwebtoken");
//import { jwt } from "jsonwebtoken";
export const createToken = async (data: any, jwtSecretKey: string) => {
  try {
    return await jwt.sign(data, jwtSecretKey);
  } catch (e) {
    console.error("Error creating token", {
      error: e,
    });
    return "Error creating token";
  }
};

export const verifyToken = async (token: string, jwtSecretKey: string) => {
  try {
    return await jwt.verify(token, jwtSecretKey);
  } catch (e) {
    console.error("Error verifiying badges", {
      error: e,
    });
    return false;
  }
};
