export const vpc = {
  local: {
    securityGroupIds: ["sg-05b1059ee9f39eb86", "sg-0acacf5883e82d6b9"],
    subnetIds: [
      "subnet-06dc8925a335070c4",
      "subnet-02a850ff8e532c30a",
      "subnet-07a7618423b33e844",
      "subnet-06512bffbecf0bd09",
      "subnet-060ea8cc7a54067cd",
      "subnet-0bc3202acb8a31eca",
    ],
  },
  prod: {
    securityGroupIds: ["sg-05b1059ee9f39eb86", "sg-0acacf5883e82d6b9"],
    subnetIds: [
      "subnet-06dc8925a335070c4",
      "subnet-02a850ff8e532c30a",
      "subnet-07a7618423b33e844",
      "subnet-06512bffbecf0bd09",
      "subnet-060ea8cc7a54067cd",
      "subnet-0bc3202acb8a31eca",
    ],
  },
  dev: {
    securityGroupIds: ["sg-05b1059ee9f39eb86", "sg-0acacf5883e82d6b9"],
    subnetIds: [
      "subnet-06dc8925a335070c4",
      "subnet-02a850ff8e532c30a",
      "subnet-07a7618423b33e844",
      "subnet-06512bffbecf0bd09",
      "subnet-060ea8cc7a54067cd",
      "subnet-0bc3202acb8a31eca",
    ],
  },
};

//VPC vpc-0e7af0187fd35692b
// Security Groups: aurora-custom-sg (active)
