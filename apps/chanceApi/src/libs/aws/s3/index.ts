import {
  S3Client,
  PutObjectCommandInput,
  PutObjectCommandOutput,
  PutObjectCommand,
} from "@aws-sdk/client-s3";

const s3Client = new S3Client({ region: "us-east-1" });
export const putObjectS3 = async (
  input: PutObjectCommandInput
): Promise<PutObjectCommandOutput> => {
  try {
    const putObjectCommand = new PutObjectCommand(input);
    return s3Client.send(putObjectCommand);
  } catch (error) {
    console.error({
      error: "Failed to put object",
      message: error,
      input: input,
    });
    throw new Error("Failed to put object");
  }
};
