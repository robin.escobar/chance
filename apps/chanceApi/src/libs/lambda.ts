import middy from "@middy/core";
import sts from "@middy/sts";
import middyJsonBodyParser from "@middy/http-json-body-parser";

export const middyfy = (handler) =>
  middy(handler)
    .use(middyJsonBodyParser())
    .use({
      before: async (request) => {},
      // before: async (lambdaHandler, next: () => void) => {
      //   console.log("in lambda handler", lambdaHandler);
      //   // const queryParams = lambdaHandler.event.queryStringParameters || {};
      //   // console.log(queryParams)
      //   // if (queryParams.credentials) {
      //   //   const credentials = await getCredentials(queryParams.credentials);
      //   //   Object.assign(lambdaHandler.context, { credentials });
      //   // }
      //   next();
      // },
    });

export const middyfyWithSTS = (policy, handler) =>
  middy(handler)
    .use(middyJsonBodyParser())
    .use(
      sts({
        fetchData: {
          assumeRole: policy,
        },
      })
    );
