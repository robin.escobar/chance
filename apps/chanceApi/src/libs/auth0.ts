import { ManagementClient } from "auth0";

let client = null;

export const getClient = (scopes: string) => {
  // if (client) return client;
  console.log("here");
  console.log(process.env.AUTH0_MANAGEMENT_CLIENT_APP_DOMAIN);
  console.log(process.env.AUTH0_MANAGEMENT_CLIENT_APP_ID);
  console.log(process.env.AUTH0_MANAGEMENT_CLIENT_APP_SECRET);
  client = new ManagementClient({
    domain: `${process.env.AUTH0_MANAGEMENT_CLIENT_APP_DOMAIN}`,
    clientId: `${process.env.AUTH0_MANAGEMENT_CLIENT_APP_ID}`,
    clientSecret: `${process.env.AUTH0_MANAGEMENT_CLIENT_APP_SECRET}`,
    // domain: process.env.AUTH0_DOMAIN,
    // clientId: process.env.AUTH0_MANAGEMENT_CLIENT_ID,
    // clientSecret: process.env.AUTH0_MANAGEMENT_CLIENT_SECRET,

    scope: scopes,
  });

  return client;
};

/** ######### ORGANIZATION ENDPOINTS ######## */

/**
 * Fetches the database connection of the given Organization
 *
 * @see https://auth0.com/docs/api/management/v2/#!/Organizations/get_enabled_connections
 *
 * @param request
 * @param response
 */
export const getOrganizationConnections = async (organizationId: string) => {
  try {
    const currentUserManagementClient = getClient(
      "read:organization_connections"
    );

    const connections =
      await currentUserManagementClient.organizations.getEnabledConnections({
        id: organizationId,
      });

    return connections;
  } catch (e) {
    console.error("Error fetching organization connections");
    console.error(e);
    throw e;
  }
};

/**
 * Creates an invitation to this organization
 *
 * @see https://auth0.com/docs/api/management/v2/#!/Organizations/post_invitations
 *
 * @param response
 */
export const createOrganizationInvitation = async (
  organizationId: string,
  inviter: { name: string },
  invitee: { email: string },
  connectionId: string,
  roles: Array<string>
) => {
  try {
    const currentUserManagementClient = getClient(
      "create:organization_invitations"
    );

    const connections =
      await currentUserManagementClient.organizations.createInvitation(
        {
          id: organizationId,
        },
        {
          inviter,
          invitee,
          client_id: process.env.AUTH0_APPRENTICESHIP_DASHBOARD_CLIENT_ID,
          connection_id: connectionId,
          roles,
        }
      );

    return connections;
  } catch (e) {
    console.error("Error creating organization invitation");
    console.error(e);
    throw e;
  }
};

/**
 * Fetches the organization members belonging to the user's Organization
 *
 * @see https://auth0.com/docs/manage-users/organizations/configure-organizations/retrieve-members
 *
 * @param request
 * @param response
 */
export const getOrganizationMembers = async (organizationId: string) => {
  try {
    const currentUserManagementClient = getClient("read:organization_members");

    const members = await currentUserManagementClient.organizations.getMembers({
      id: organizationId,
    });

    return members;
  } catch (e) {
    console.error("Error fetching organization members");
    console.error(e);
    throw e;
  }
};

/** ######### ROLE ENDPOINTS ######## */

/**
 * Fetches the roles available to us
 *
 * @see https://auth0.com/docs/manage-users/organizations/configure-organizations/retrieve-members
 *
 * @param name : The role name
 * @param response
 */
export const getRoleByName = async (name: string) => {
  try {
    const currentUserManagementClient = getClient("read:roles");

    const role = await currentUserManagementClient.getRoles({
      name_filter: name,
    });

    return role;
  } catch (e) {
    console.error("Error fetching organization members");
    console.error(e);
    throw e;
  }
};

/** ######### USER ENDPOINTS ######## */

/**
 * Fetches the user from Auth0 Management API
 * using the currently logged in user's session.
 *
 * This allows us to view the user's app_metadata,
 * not available with nextjs-auth0
 *
 * @see https://auth0.com/docs/manage-users/user-accounts/metadata
 *
 * @param userId
 */
export const getUser = async (userId: string) => {
  try {
    const currentUserManagementClient = getClient("read:user");

    const user = await currentUserManagementClient.getUser({
      id: userId,
    });

    return user;
  } catch (e) {
    console.error("Error fetching user from auth0");
    console.error(e);
    throw e;
  }
};

/**
 * Updates user app_metadata using Auth0 Management API
 *
 * @see https://auth0.com/docs/manage-users/user-accounts/metadata
 *
 * @param request
 * @param response
 */
export const updateUserAppMetadata = async (userId: string, metadata: {}) => {
  try {
    const currentUserManagementClient = getClient(
      "read:users_app_metadata create:users_app_metadata update:users_app_metadata"
    );

    const user = await currentUserManagementClient.updateAppMetadata(
      {
        id: userId,
      },
      metadata
    );

    return user;
  } catch (e) {
    console.error("Error updating user app metadata");
    console.error(e);
    throw e;
  }
};
