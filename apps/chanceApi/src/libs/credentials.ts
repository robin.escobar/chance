import { STSClient, AssumeRoleCommand } from "@aws-sdk/client-sts";

const stsClient = new STSClient({});

const generatePolicy = (statements: Array<any>) => {
  const policyDocument = {
    Version: "2012-10-17", // default version
    Statement: statements,
  };

  return policyDocument;
};

const getCredentials = async (policies, roleArn = process.env.STSAssumeRoleARN) => {
  const command = new AssumeRoleCommand({
    RoleArn: roleArn,
    RoleSessionName: `userSession${new Date().getTime().toString()}`,
    Policy: JSON.stringify(generatePolicy(policies)),
  });
  const data = await stsClient.send(command);

  const { Credentials } = data;
  return {
    accessKeyId: Credentials!.AccessKeyId,
    secretAccessKey: Credentials!.SecretAccessKey,
    sessionToken: Credentials!.SessionToken,
  };
};

export default getCredentials;
