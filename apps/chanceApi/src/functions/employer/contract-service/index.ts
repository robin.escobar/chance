import handlerPath from "../../../libs/handler-resolver";
import { vpc } from "@chance/awsconfig/vpc";

export const contractService = {
  handler: `${handlerPath(__dirname)}/handler.contractService`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "post",
        path: "/contract",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const getAllContractServices = {
  handler: `${handlerPath(__dirname)}/handler.getAllContractServices`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "get",
        path: "/contract",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};
