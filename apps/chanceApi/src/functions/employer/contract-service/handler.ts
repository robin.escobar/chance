import { APIGatewayProxyResult } from "aws-lambda";
import { formatJSONResponse } from "@libs/api-gateway";
import middy from "@middy/core";

import {
  create as createContractServiceQuery,
  deleteById as deleteByIdServiceQuery,
  update as updateServiceQuery,
  getAllContractServices as getAllContractServicesQuery,
  getAllContractServicesForEmployeeID,
  getAllContractServicesForEmployerID,
} from "@chance/db/queries/employer/contrat-service";

import { ContractService } from "@types/ContractService";

export const contractService = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const body = JSON.parse(event.body);
      const contractService: ContractService = {
        ...body,
      };

      const response = await createContractServiceQuery(contractService);
      console.log({ rowCount: response });

      return formatJSONResponse({
        status: 200,
        message: "Service Contracted",
      });
    } catch (e) {
      console.error("Error getting service", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error getting service",
      });
    }
  }
).use({
  before: async (request) => {
    //TODO LOGIC VERIFICATION
    // console.log({ Event: request.event });
    // const { organizationId } = request.event.requestContext.authorizer;
    // Object.assign(request.event.requestContext.authorizer, {
    //   database_credentials: organizationId,
    // });
  },
});

export const getAllContractServices = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const response = await getAllContractServicesQuery();
      console.log({ data: response });

      return formatJSONResponse({
        status: 200,
        data: response,
      });
    } catch (e) {
      console.error("Error getting contract services", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error getting ccontract services",
      });
    }
  }
).use({
  before: async (request) => {
    //TODO LOGIC VERIFICATION
    // console.log({ Event: request.event });
    // const { organizationId } = request.event.requestContext.authorizer;
    // Object.assign(request.event.requestContext.authorizer, {
    //   database_credentials: organizationId,
    // });
  },
});
