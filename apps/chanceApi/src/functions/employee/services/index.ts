import handlerPath from "../../../libs/handler-resolver";
import { vpc } from "@chance/awsconfig/vpc";

export const createService = {
  handler: `${handlerPath(__dirname)}/handler.createService`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "post",
        path: "/service",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const deleteService = {
  handler: `${handlerPath(__dirname)}/handler.deleteService`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "delete",
        path: "/service/{serviceId}",
        request: {
          // schemas: {
          //   "application/json": createUserRelationshipSchema,
          // },
          parameters: {
            paths: {
              serviceId: true,
            },
          },
        },
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const updateService = {
  handler: `${handlerPath(__dirname)}/handler.updateService`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "put",
        path: "/service/{serviceId}",
        request: {
          // schemas: {
          //   "application/json": createUserRelationshipSchema,
          // },
          parameters: {
            paths: {
              serviceId: true,
            },
          },
        },
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const getAllServices = {
  handler: `${handlerPath(__dirname)}/handler.getAllServices`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "get",
        path: "/service",
        request: {
          // schemas: {
          //   "application/json": createUserRelationshipSchema,
          // },
        },
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

//getAllServicesForUserID

// export const getAllServicesForUserID = {
//   handler: `${handlerPath(__dirname)}/handler.getAllServicesForUserID`,
//   vpc: vpc[process.env.ENVIRONMENT ?? "local"],
//   // role: "LambdaDynamoDBCallerRole",
//   events: [
//     {
//       http: {
//         method: "get",
//         path: "/service/jeje",
//         request: {
//           // schemas: {
//           //   "application/json": createUserRelationshipSchema,
//           // },
//           // parameters: {
//           //   paths: {
//           //     userId: true,
//           //   },
//           // },
//         },
//         cors: {
//           origin: "*",
//         },
//       },
//     },
//   ],
// };
