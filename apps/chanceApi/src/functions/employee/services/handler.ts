import { APIGatewayProxyResult } from "aws-lambda";
import { formatJSONResponse } from "@libs/api-gateway";
import middy from "@middy/core";

import {
  create as createServiceQuery,
  deleteById as deleteByIdServiceQuery,
  update as updateServiceQuery,
  getAllServices as getAllServicesQuery,
  getAllServicesForUserID as getAllServicesForUserIDQuery,
} from "@chance/db/queries/employee/services";

import { Service } from "@types/Service";

export const createService = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const body = JSON.parse(event.body);
      const service: Service = {
        description: body.description,
        details: body.details,
        fk_user_id: body.fk_user_id,
        fk_type_service_id: body.fk_type_service_id,
      };

      const response = await createServiceQuery(service);
      console.log({ rowCount: response });

      return formatJSONResponse({
        status: 200,
        message: "Service Created",
      });
    } catch (e) {
      console.error("Error creating service", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error creating user",
      });
    }
  }
).use({
  before: async (request) => {
    //TODO LOGIC VERIFICATION
    // console.log({ Event: request.event });
    // const { organizationId } = request.event.requestContext.authorizer;
    // Object.assign(request.event.requestContext.authorizer, {
    //   database_credentials: organizationId,
    // });
  },
});

export const deleteService = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const { serviceId } = event.pathParameters;
      const response = await deleteByIdServiceQuery(serviceId);

      console.log({ response });

      return formatJSONResponse({
        status: 200,
        message: "Service deleted",
      });
    } catch (e) {
      console.error("Error deleting service", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error deleting service",
      });
    }
  }
);

export const updateService = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const { serviceId } = event.pathParameters;
      const body = JSON.parse(event.body);

      const service: Service = {
        description: body.description ?? "",
        details: body.details ?? "",
        status: body.status ?? "",
      };
      const response = await updateServiceQuery(serviceId, service);

      console.log({ response });

      return formatJSONResponse({
        status: 200,
        message: "Service deleted",
      });
    } catch (e) {
      console.error("Error deleting service", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error deleting service",
      });
    }
  }
);

export const getAllServices = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const services = await getAllServicesQuery();

      return formatJSONResponse({
        services,
      });
    } catch (e) {
      console.error("Error deleting service", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error deleting service",
      });
    }
  }
);

export const getAllServicesForUserID = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const { userId } = event.pathParameters;
      const users = await getAllServicesForUserIDQuery(userId);
      return formatJSONResponse({
        users,
      });
    } catch (e) {
      console.error("Error getting usersss", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error getting users",
      });
    }
  }
);
