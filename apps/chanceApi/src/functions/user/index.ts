import handlerPath from "../../libs/handler-resolver";
import { vpc } from "@chance/awsconfig/vpc";

export const createUser = {
  handler: `${handlerPath(__dirname)}/handler.createUser`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],

  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "post",
        path: "/user",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const updateUser = {
  handler: `${handlerPath(__dirname)}/handler.updateUser`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],

  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "put",
        path: "/user/{userId}",
        request: {
          // schemas: {
          //   "application/json": createUserRelationshipSchema,
          // },
          parameters: {
            paths: {
              userId: true,
            },
          },
        },
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const getAllUsers = {
  handler: `${handlerPath(__dirname)}/handler.getAll`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],

  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "get",
        path: "/user",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};
