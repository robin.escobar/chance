import { APIGatewayProxyResult, APIGatewayProxyEvent } from "aws-lambda";
import { formatJSONResponse } from "@libs/api-gateway";
import middy from "@middy/core";
import { sendPinpointSMS } from "@chancelibs/aws/pin-point";
import { sendMail } from "@chancelibs/nodemailer";
import { createUserTemplate } from "../../../../../packages/templates/email";
import {
  create as createUserService,
  update as updateUserService,
  getAll as getAllUsersQuery,
} from "@chance/db/queries/user";
import { User } from "@types/User";
import { encrypt } from "@chancelibs/encrypt";

export const createUser = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const verificationCode = Math.round(Math.random() * 999999);
      const body = JSON.parse(event.body);
      const user: User = {
        pass: await encrypt(body.pass, 10),
        email: body?.email ?? "",
        phone_number: body.phone_number ?? null,
        role: body?.role,
        verification_code: verificationCode,
      };

      const response = await createUserService(user);

      if (user.email) {
        //TODO use EventBridge
        const message2 = {
          from: "delivery@chance.com.gt",
          to: user.email,
          subject: "Ejemplo de asunto de correo",
          text: "Plaintext version of the message",
          html: `
<html>
  <head>
    <style>
      body {
        display: flex;
        justify-content: center;
      }
      .main {
        box-shadow: 2px 2px 5px #c2bdbc;
        border-radius: 20px;
        width: 550px;
        height: 200px;
        border: 1px solid #c2bdbc;
      }
      .head {
        margin: 20px 0 0 20px;
        width: 100%;
      }

      .line {
        border-top: 2px solid #d8d4d3;
        margin: 0px 25px 0 75px;
      }

      .avatar {
        vertical-align: middle;
        width: 50px;
        height: 35px;
        border-radius: 50%;
      }

      .taskicon {
        vertical-align: middle;
        width: 30px;
        height: 30px;
        border-radius: 50%;
      }

      table {
        margin: 30px 0 0 50px;
        table-layout: fixed;
        width: 500px;
        border-collapse: separate;
        border-spacing: 10px 5px;
      }

      div.center {
        margin-top: 50px;
        text-align: center;
      }
      .btn {
        text-decoration: none;
        box-shadow: 2px 2px 5px #c2bdbc;
        border-radius: 10px;
        border: none;
        background-color: black;
        padding: 14px 28px;
        font-size: 16px;
        cursor: pointer;
        display: inline-block;
      }

      .btntag {
        border-radius: 10px;
        border: none;
        background-color: #dbb8b8;
        padding: 3px 6px;
        font-size: 12px;
        cursor: pointer;
        display: inline-block;
        margin-top: 5px;
      }

      .btn:hover {
        background-color: #75716f;
        color: white;
      }
    </style>
  </head>
  <body>
    <div class="main">
      <div class="head">
        <img  src="https://i.postimg.cc/ZqmXfywg/Whats-App-Image-2023-01-31-at-5-09-11-PM.jpg"  alt="Avatar" class="avatar" />
        <label><b> Te damos la bienvenida a Chance</b></label>
      </div>
      <div class="line"></div>

      <table>
        <tr>
          <td style="width: 20%; color: #75716f">
            <b>Codigo de verificación</b>
          </td>
          <td style="width: 80%"><h2>${user.verification_code}</h2></td>
        </tr>
      </table>
    </div>
  </body>
</html>`,
        };
        await sendMail(message2);
      } else if (user.phone_number) {
        //TODO use Eventbridge
        const smsParameters = {
          ApplicationId: process.env.PINPOINT_APPLICATION_ID,
          MessageRequest: {
            Addresses: {
              [user.phone_number]: {
                ChannelType: "SMS",
              },
            },

            MessageConfiguration: {
              SMSMessage: {
                Body: `Codigo de verificación:  ${user.verification_code}.`,
                Keyword: process.env.PINPOINT_KEYWORD,
                MessageType: "TRANSACTIONAL",
                OriginationNumber: process.env.PINPOINT_ORIGINATION_NUMBER,
              },
            },
          },
        };
        const data = await sendPinpointSMS(smsParameters);
        console.log("data response", { data: data?.MessageResponse?.Result });
      }
      return formatJSONResponse({
        status: 200,
        message: "User Created",
      });
    } catch (e) {
      console.error("Error creating user", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error creating user",
      });
    }
  }
).use({
  before: async (request) => {
    //TODO LOGIC VERIFICATION
    // console.log({ Event: request.event });
    // const { organizationId } = request.event.requestContext.authorizer;
    // Object.assign(request.event.requestContext.authorizer, {
    //   database_credentials: organizationId,
    // });
  },
});

export const updateUser = middy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const body: User = JSON.parse(event.body);
      const { userId } = event.pathParameters;
      console.log(body);
      const response = await updateUserService(userId, body);

      console.log({ response });

      return formatJSONResponse({
        status: 200,
        message: "User updated",
      });
    } catch (e) {
      console.error("Error updating user", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error updating badges",
      });
    }
  }
);

export const getAll = middy(async (event): Promise<APIGatewayProxyResult> => {
  try {
    const users = await getAllUsersQuery();
    return formatJSONResponse({
      users,
    });
  } catch (e) {
    console.error("Error getting usersss", {
      error: e,
    });

    return formatJSONResponse({
      status: 500,
      message: "Error getting users",
    });
  }
});

//getAllServicesForUserID
