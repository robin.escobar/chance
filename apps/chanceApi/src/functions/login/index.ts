import handlerPath from "../../libs/handler-resolver";
import { vpc } from "@chance/awsconfig/vpc";

export const singIn = {
  handler: `${handlerPath(__dirname)}/handler.singIn`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "post",
        path: "/login",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const resetPassword = {
  handler: `${handlerPath(__dirname)}/handler.resetPassword`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "post",
        path: "/reset",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};
