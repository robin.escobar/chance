import { APIGatewayProxyResult } from "aws-lambda";
import { formatJSONResponse } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import { getByEmail } from "@chance/db/queries/user";
import { create as createLogin } from "@chance/db/queries/logins";
import { createToken } from "@chancelibs/jwt";
import { User } from "@types/User";
import { compare } from "@chancelibs/encrypt";
import { generatePassword } from "@chancelibs/password";

export const singIn = middyfy(async (event): Promise<APIGatewayProxyResult> => {
  try {
    const body: any = event.body;
    const user: User[] = await getByEmail(body.email);

    //validate user and password
    if (
      body.email === user[0].email ||
      body.phone_number === user[0].phone_number
    ) {
      const passIsCorrect = await compare(body.pass, user[0].pass);

      if (passIsCorrect) {
        const data = {
          user: {
            full_name: user[0]?.full_name,
            role: user[0]?.role,
            id: user[0]?.user_id,
          },
          time: Date(),
        };
        const token = await createToken(data, process.env.JWT_SECRET_KEY);
        await createLogin({ user_id: user[0].user_id, token: token });
        //todo use redis

        return formatJSONResponse({ data, token });
      } else {
        return formatJSONResponse({
          status: 403,
          message: "User or password incorrect 1",
        });
      }
    } else {
      return formatJSONResponse({
        status: 403,
        message: "User or password incorrect 2",
      });
    }
  } catch (e) {
    console.error("Error on login", {
      error: e,
    });

    return formatJSONResponse({
      status: 500,
      message: "Error on login",
    });
  }
});

export const resetPassword = middyfy(
  async (event): Promise<APIGatewayProxyResult> => {
    try {
      const body: any = event.body;
      if (body.email) {
        console.log("Passoword: ", generatePassword());
        //TODO... update password on database and send emial o sms
      }
    } catch (e) {
      console.error("Error on login", {
        error: e,
      });

      return formatJSONResponse({
        status: 500,
        message: "Error on login",
      });
    }
  }
);
