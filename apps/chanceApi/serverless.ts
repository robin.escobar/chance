import type { AWS } from "@serverless/typescript";
import { vpc } from "@chance/awsconfig/vpc";
import { pinPoint } from "@chance/awsconfig/pinpoint";
import {
  createService,
  deleteService,
  updateService,
  getAllServices,
  // getAllServicesForUserID,
} from "./src/functions/employee/services";
import {
  contractService,
  getAllContractServices,
  // getAllServicesForUserID,
} from "./src/functions/employer/contract-service";
import { singIn, resetPassword } from "./src/functions/login";
import { createUser, updateUser, getAllUsers } from "./src/functions/user";
const serverlessConfiguration: AWS = {
  service: "chance-api",
  frameworkVersion: "3",
  plugins: ["serverless-esbuild", "serverless-offline"],
  provider: {
    name: "aws",
    runtime: "nodejs14.x",
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      ENVIRONMENT: "${sls:stage}",
      JWT_SECRET_KEY: "jwtSecretKey",
      RONDAL_CRYPTS_JS: "rondalCcryptjs",
      PINPOINT_APPLICATION_ID:
        "${self:custom.AWS.pinpoint.${sls:stage}.pinPointApplicationId}",
      PINPOINT_ORIGINATION_NUMBER:
        "${self:custom.AWS.pinpoint.${sls:stage}.pinPointOriginNumber}",
      PINPOINT_KEYWORD: "${self:custom.AWS.pinpoint.${sls:stage}.keyword}",
    },
    vpc: vpc[process.env.ENVIRONMENT ?? "local"],
    iam: {
      role: {
        statements: [],
      },
    },
  },
  functions: {
    //Login
    singIn,
    resetPassword,
    //Users
    createUser,
    updateUser,
    getAllUsers,
    //Service
    createService,
    deleteService,
    updateService,
    getAllServices,
    // getAllServicesForUserID,
    //contract
    contractService,
    getAllContractServices,
  },
  package: { individually: true },
  custom: {
    AWS: {
      pinpoint: {
        ...pinPoint,
      },
    },
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: [
        "aws-sdk",
        "better-sqlite3",
        "mysql2",
        "oracledb",
        "sqlite3",
        "tedious",
        "pg-native",
        "mysql",
        "pg-query-stream",
      ],
      target: "node14",
      define: { "require.resolve": undefined },
      platform: "node",
      concurrency: 10,
    },
  },
  resources: {
    Resources: {},
  },
};
module.exports = serverlessConfiguration;
