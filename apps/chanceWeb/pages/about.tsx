// import { MDXLayoutRenderer } from '@/components/MDXComponents'
import { InferGetStaticPropsType } from 'next'
import { allAuthors } from 'contentlayer/generated'

const DEFAULT_LAYOUT = 'AuthorLayout'

export const getStaticProps = async () => {
  const author = allAuthors.find((p) => p.slug === 'about')
  return { props: { author } }
}

export default function About({ author }: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <div className="divide-y divide-gray-200 dark:divide-gray-700">
      <div className="space-y-2 pt-6 pb-8 md:space-y-5">
        <h1 className="text-3xl font-extrabold leading-9 tracking-tight text-gray-900 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-6xl md:leading-14">
          Sobre Nosotros
        </h1>
      </div>
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="sm:text-center">
          <p className="mx-auto mt-6 max-w-2xl text-lg leading-8 text-gray-600">
            “Chance” es una plataforma que sirve principalmente para agilizar el proceso de búsqueda
            de personas que realizan empleo informal (jardineros, empleadas domesticas, estilistas y
            albañiles) con personas que necesiten el servicio. Ademas como una estrategia de
            marketing para facilitar el contacto de pequeñas empresas/comercios con sus clientes.
          </p>
          <br />
          <h3 className="text-3xl font-extrabold leading-9 tracking-tight text-gray-900 dark:text-gray-100 sm:text-2xl sm:leading-10 md:text-4xl md:leading-14">
            Definición de "Chance"
          </h3>
          <p className="mx-auto mt-6 max-w-2xl text-lg leading-8 text-gray-600">
            Real academia española: Voz tomada del francés o del inglés chance, que significa
            ‘oportunidad’.
          </p>
        </div>
      </div>
    </div>
  )
}
