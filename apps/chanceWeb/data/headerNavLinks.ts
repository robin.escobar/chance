const headerNavLinks = [
  { href: '/blog', title: 'Blog' },
  //  { href: '/tags', title: 'Tags' },
  { href: '/projects', title: 'Proyectos' },
  { href: '/about', title: 'Sobre Nosotros' },
]

export default headerNavLinks
