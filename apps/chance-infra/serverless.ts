import {
  migrateLatest,
  listMigrations,
  rollback,
} from "./src/functions/migrations";
import { vpc } from "@chance/awsconfig/vpc";

const serverlessConfiguration = {
  service: "chance-migrations",
  frameworkVersion: "3",
  plugins: ["serverless-esbuild", "serverless-offline"],
  provider: {
    name: "aws",
    runtime: "nodejs14.x",
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: { ENVIRONMENT: "${sls:stage}" },
    vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  },
  functions: { migrateLatest, listMigrations, rollback },
  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: [
        "aws-sdk",
        "better-sqlite3",
        "mysql2",
        "oracledb",
        "sqlite3",
        "tedious",
        "pg-native",
        "mysql",
        "pg-query-stream",
      ],
      target: "node14",
      define: { "require.resolve": undefined },
      platform: "node",
      concurrency: 10,
    },
  },
  resources: {
    Resources: {},
    Outputs: {},
  },
};
module.exports = serverlessConfiguration;
