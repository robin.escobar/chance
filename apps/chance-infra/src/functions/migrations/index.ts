const handlerPath = (context: string) =>
  `${context.split(process.cwd())[1].substring(1).replace(/\\/g, "/")}`;
import { vpc } from "@chance/awsconfig/vpc";

export const migrateLatest = {
  handler: `${handlerPath(__dirname)}/handler.migrateLatest`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "post",
        path: "/migrations/latest",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const rollback = {
  handler: `${handlerPath(__dirname)}/handler.rollback`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "post",
        path: "/migrations/rollback",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};

export const listMigrations = {
  handler: `${handlerPath(__dirname)}/handler.listMigrations`,
  vpc: vpc[process.env.ENVIRONMENT ?? "local"],
  // role: "LambdaDynamoDBCallerRole",
  events: [
    {
      http: {
        method: "get",
        path: "/migrations/list",
        cors: {
          origin: "*",
        },
      },
    },
  ],
};
