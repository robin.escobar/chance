import {
  migrateLatest as migrateLatestPackage,
  rollback as rollbackPackage,
  listMigrations as listMigratiosPackage,
} from "@chance/db";

export const migrateLatest = async (event) => {
  await migrateLatestPackage();
};

export const rollback = async (event) => {
  await rollbackPackage();
};

export const listMigrations = async (event) => {
  await listMigratiosPackage();
};
